# frozen_string_literal: true

describe Dependabot::Projects::Sync, integration: true, epic: :services, feature: :dependabot do
  subject(:sync) { described_class }

  let(:gitlab) { instance_double("Gitlab::Client", projects: projects_response) }
  let(:projects_response) { instance_double("Gitlab::PaginatedResponse") }

  let(:cron) { "0 1 * * * UTC" }
  let(:project_name) { random_name }
  let(:project_id) { random_id }
  let(:default_branch) { "main" }
  let(:projects) { [project] }

  let(:project) do
    Gitlab::ObjectifiedHash.new(
      id: project_id,
      path_with_namespace: project_name,
      default_branch: default_branch
    )
  end

  let(:config) do
    [
      {
        package_ecosystem: "npm",
        directory: "/",
        cron: cron
      },
      {
        package_ecosystem: "bundler",
        directory: "/",
        cron: cron
      }
    ]
  end

  let(:jobs) do
    [
      Sidekiq::Cron::Job.new(name: "#{project_name}:bundler:/", cron: cron),
      Sidekiq::Cron::Job.new(name: "#{project_name}:npm:/", cron: cron)
    ]
  end

  let(:saved_project) { Project.new(name: project_name, id: project_id) }

  def random_id
    Faker::Number.number(digits: 10)
  end

  def random_name
    Faker::Alphanumeric.unique.alpha(number: 15)
  end

  before do
    allow(Gitlab::Client).to receive(:new) { gitlab }
    allow(projects_response).to receive(:auto_paginate).and_yield(*projects)
    allow(Dependabot::Projects::Creator).to receive(:call).with(project.path_with_namespace) { saved_project }
    allow(Cron::JobSync).to receive(:call).with(saved_project)

    allow(Dependabot::Config::Fetcher).to receive(:call)
      .with(
        project.path_with_namespace, branch: project.default_branch, update_cache: true
      )
      .and_return(config)
  end

  context "with ignored project" do
    before do
      allow(AppConfig).to receive(:project_registration_namespace).and_return("test")
    end

    it "skips project" do
      sync.call

      expect(Dependabot::Config::Fetcher).not_to have_received(:call)
    end
  end

  context "with non existing project", :aggregate_failures do
    context "without configuration" do
      before do
        allow(Dependabot::Config::Fetcher).to receive(:call).and_raise(Dependabot::Config::MissingConfigurationError)
      end

      it "skips registering project" do
        sync.call

        expect(Dependabot::Projects::Creator).not_to have_received(:call).with(project_name)
        expect(Cron::JobSync).not_to have_received(:call).with(saved_project)
      end
    end

    context "with configuration" do
      it "registers project" do
        sync.call

        expect(Dependabot::Projects::Creator).to have_received(:call).with(project_name)
        expect(Cron::JobSync).to have_received(:call).with(saved_project)
      end
    end

    context "without default_branch" do
      let(:default_branch) { nil }

      it "skips project" do
        sync.call

        expect(Dependabot::Projects::Creator).not_to have_received(:call)
      end
    end
  end

  context "with existing project" do
    before do
      saved_project.save!
    end

    context "without config" do
      before do
        allow(Dependabot::Config::Fetcher).to receive(:call).and_raise(Dependabot::Config::MissingConfigurationError)
        allow(Dependabot::Projects::Remover).to receive(:call).with(project_name)
      end

      it "removes project" do
        sync.call

        expect(Dependabot::Projects::Remover).to have_received(:call).with(project_name)
      end
    end

    context "with out of sync jobs", :aggregate_failures do
      before do
        allow(Sidekiq::Cron::Job).to receive(:all).and_return([jobs[0]])
      end

      it "syncs project and jobs" do
        sync.call

        expect(Dependabot::Projects::Creator).to have_received(:call).with(project_name)
        expect(Cron::JobSync).to have_received(:call).with(saved_project)
      end
    end

    context "with jobs in sync", :aggregate_failures do
      before do
        allow(Sidekiq::Cron::Job).to receive(:all).and_return(jobs)
      end

      it "skips project" do
        sync.call

        expect(Dependabot::Projects::Creator).not_to have_received(:call).with(project_name)
        expect(Cron::JobSync).not_to have_received(:call).with(saved_project)
      end
    end

    context "with renamed project", :aggregate_failures do
      let(:new_name) { random_name }

      let(:project) do
        Gitlab::ObjectifiedHash.new(
          id: project_id,
          path_with_namespace: new_name,
          default_branch: default_branch
        )
      end

      before do
        allow(Cron::JobRemover).to receive(:call)
      end

      it "renames existing project" do
        sync.call

        expect(saved_project.reload.name).to eq(new_name)
        expect(Cron::JobRemover).to have_received(:call).with(project_name)
        expect(Cron::JobSync).to have_received(:call).with(saved_project)
      end
    end
  end
end
